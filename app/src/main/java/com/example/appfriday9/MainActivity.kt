package com.example.appfriday9

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.example.appfriday9.databinding.ActivityMainBinding
import com.example.appfriday9.model.ItemModel

class MainActivity : AppCompatActivity() {


    private lateinit var myAdapter: RecyclerAdapter


    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        initRecycler()
        setContentView(binding.root)


    }

    private fun initRecycler() {

        myAdapter = RecyclerAdapter(
            listOf(
                ItemModel(1),
                ItemModel(2),
                ItemModel(3),
                ItemModel(4),
                ItemModel(5),
                ItemModel(6),
                ItemModel(7),
                ItemModel(8),
                ItemModel(9),
            )
        )

        binding.recycler.layoutManager = GridLayoutManager(this, 3)
        binding.recycler.adapter = myAdapter

    }

}