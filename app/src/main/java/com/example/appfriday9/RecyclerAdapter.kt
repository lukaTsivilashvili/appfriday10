package com.example.appfriday9

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.appfriday9.databinding.ItemRecyclerBinding
import com.example.appfriday9.model.ItemModel
import kotlin.random.Random

class RecyclerAdapter(val items:List<ItemModel>): RecyclerView.Adapter<RecyclerAdapter.MyViewHolder>() {

    inner class MyViewHolder(private val binding: ItemRecyclerBinding):RecyclerView.ViewHolder(binding.root){

        private lateinit var model: ItemModel

        fun onBind(){

            model = items[adapterPosition]

            if (binding.recyclerItem.text != Random.nextInt(9).toString()){
                binding.recyclerItem.text = Random.nextInt(9).toString()
            }

        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = MyViewHolder(
        ItemRecyclerBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
    )

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.onBind()
    }

    override fun getItemCount() = items.size

}